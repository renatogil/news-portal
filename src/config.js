import defaultPic from './assets/noimage.png';

export const apiKey = 'XQHZq601btecyxFX65xWnaMmI74mprvX';
export const apiURL = 'https://api.nytimes.com/svc/';
export const siteBaseUrl = 'https://www.nytimes.com/';
export const debounceDefaultTime = 500;
export const sections = [{
        value: 'home',
        text: 'All'
    },
    {
        value: 'arts',
        text: 'Arts'
    },
    {
        value: 'automobiles',
        text: 'Automobiles'
    },
    {
        value: 'books',
        text: 'Books'
    },
    {
        value: 'business',
        text: 'Business'
    },
    {
        value: 'fashion',
        text: 'Fashion'
    },
    {
        value: 'food',
        text: 'Food'
    },
    {
        value: 'health',
        text: 'Health'
    },
    {
        value: 'insider',
        text: 'Insider'
    },
    {
        value: 'magazine',
        text: 'Magazine'
    },
    {
        value: 'movies',
        text: 'Movies'
    },
    {
        value: 'national',
        text: 'National'
    },
    {
        value: 'opinion',
        text: 'Opinion'
    },
    {
        value: 'politics',
        text: 'Politics'
    },
    {
        value: 'realestate',
        text: 'Real Estate'
    },
    {
        value: 'science',
        text: 'Science'
    },
    {
        value: 'sports',
        text: 'Sports'
    },
    {
        value: 'sundayreview',
        text: 'Sunday Review'
    },
    {
        value: 'technology',
        text: 'Technology'
    },
    {
        value: 'theater',
        text: 'Theater'
    },
    {
        value: 'tmagazine',
        text: 'T Magazine'
    },
    {
        value: 'travel',
        text: 'Travel'
    },
    {
        value: 'upshot',
        text: 'Upshot'
    },
    {
        value: 'world',
        text: 'World'
    }
];

export default {
    apiKey,
    apiURL,
    siteBaseUrl,
    sections,
    defaultPic,
    debounceDefaultTime
};
