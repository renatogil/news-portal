import Article from '@/components/Article';
import Vue from 'vue';

const localVue = createLocalVue();

describe('Article.vue', () => {

  it('has a getField hook', () => {
    expect(typeof Article.getField).toBe('function');
  })

})