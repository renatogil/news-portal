# news-portal

This is a news reader fed by the New York Times public API (http://developer.nytimes.com/).

## Requirements
This app has (or should have, WIP)
* A landing page with the last news;
* The landing page should have a search;
* The landing page should have filters and pagination.
* A details page where you can view in detail specific news.

## Notes
* Article search API has pagination implemented https://developer.nytimes.com/docs/articlesearch-product/1/overview
* Section (or "Top Stories" as they call it) API doesn't have pagination implemented so you won't be seeing that in the app
  
## Project setup

If you're using nvm, please run `nvm use` to switch to the proper node version. If the desired version isn't installed, you'll be
you'll be asked to install it manually. 
In the end you should be using node 12.

Run 
```
npm install
```
to install all project dependencies. Add `--verbose` to the comand for a better understand of what's going on.


### Compiles and hot-reloads for development
Run
```
npm run serve
```
to start the app. Follow the instructions in the output. If everything is OK you'll be prompted 
to open your browser at http://localhost:8080

### Compiles and minifies for production
```
npm run build
```

~~### Run your unit tests~~
I've started to implement them but haven't finished anything consistent enough.
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### TODO
* implement unit tests
* implement integration tests
* implement e2e tests
* provide a docker packaging
